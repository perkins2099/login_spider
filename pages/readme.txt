You can read back in the yaml files using this command directly into a mechanize object.

mechanize_object = YAML.load(File.read('/path/to/file.extension'))


Make sure this class is available:

class CachingMechanize < Mechanize
  def get(uri, parameters = [], referer = nil, headers = {})
    WebCache.with_web_cache(uri.to_s) { super }
  end
end

Solution:
http://stackoverflow.com/questions/11608127/writing-a-caching-version-of-mechanize
