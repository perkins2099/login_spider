require 'httparty'
require 'fileutils'
require 'csv'
require 'rubygems'
require 'mechanize'
require 'yaml'
require 'pry'

class SchemeHandler
  def call(link, page) ; link ; end
end

#Identify text I want to consider a sign in page
SIGN_IN_PAGES = ["signin","sign in","sign_in","login","log in", "log_in"]
IGNORE = ['ru','cn','in','de','jp','it','sx','br','hk','es','tr','ph','ro','ng','fr','ca','pl','gr','vn','tw']

#Make appropriate directories
dir = ["pages","pages/main_pages","pages/login_pages"]
dir.each do |d|
  unless File.directory?(d)
    FileUtils.mkdir_p(d)
  end
end

 index = 0

 info = {}
 CSV.foreach("top-1m.csv", headers: true) do |row|
    begin
        puts info
        info = {}
        index = index + 1
        domain = row["url"].split('/')[0]
        info[:index] = index
        info[:domain] = domain
        next if IGNORE.include?(domain.split(".")[-1])
        main_html = "pages/main_pages/" + domain+"_main.html"
        main_mech = "pages/main_pages/" + domain+"_main.yaml"
        login_html = "pages/login_pages/" + domain+"_login.html"
        login_mech = "pages/login_pages/" + domain+"_login.yaml"

        #Next item if these files exist
        info[:already_processed] = true
        next if File.exist?(main_html) && File.exist?(main_mech)
        info[:already_processed] = false

        url = "http://" + row["url"]
        page = nil

        begin
          agent = Mechanize.new
          agent.scheme_handlers['http']      = SchemeHandler.new
          agent.scheme_handlers['https']     = SchemeHandler.new
          agent.scheme_handlers['relative']  = SchemeHandler.new
          agent.scheme_handlers['file']      = SchemeHandler.new

          agent.read_timeout=4
          agent.open_timeout=4
          agent.user_agent_alias = 'Mac Safari'
          page = agent.get(url)
        rescue => ex
          puts ex.inspect
          info[:error] = ex.inspect
          next
        end

        info[:hp_response_code] = page.code.to_i
        #Valid URL with valid Response
        next if page.code.to_i != 200

        #puts index.to_s.ljust(10) + "-" + main_html

        File.open(main_html, 'w') { |file| file.write(page.body) }
        File.open(main_mech, 'w') { |file| file.write(YAML.dump(page)) }
        #m = YAML.load(File.read('/path/to/file.extension'))

        #Locate a Login Path
        login_page = nil
        page.links.each do |link|
          SIGN_IN_PAGES.each do |txt|
            if link.text && link.text.downcase.include?(txt) && link.href && link.href != ("#") && !link.href.include?("javascript")
              login_page = link
            end
            if link.href && link.href.downcase.include?(txt) && !login_page && link.href != ("#") && !link.href.include?("javascript")
              login_page = link
            end
          end
          break if login_page
        end
        #Next if there is no login page

        next if !login_page 
        lp = login_page.href
        unless lp =~ /:\/\//
          lp = "http://#{domain}#{lp}"
        end
        info[:lp_url] = lp

        begin
          agent = Mechanize.new
          agent.scheme_handlers['http']      = SchemeHandler.new
          agent.scheme_handlers['https']     = SchemeHandler.new
          agent.scheme_handlers['relative']  = SchemeHandler.new
          agent.scheme_handlers['file']      = SchemeHandler.new
          agent.read_timeout=4
          agent.open_timeout=4
          agent.user_agent_alias = 'Mac Safari'
          page = agent.get(lp)
        rescue => ex
          puts ex.inspect
          info[:error] = ex.inspect
          next
        end

        #Valid URL with valid Response
        info[:lp_code] = page.code.to_i
        next if page.code.to_i != 200
        #puts index.to_s.ljust(10) + "-" + login_html    
        #puts login_mech

        File.open(login_html, 'w') { |file| file.write(page.body) }
        File.open(login_mech, 'w') { |file| file.write(YAML.dump(page)) }
        #m = YAML.load(File.read('/path/to/file.extension'))
    rescue => ex
        puts "Fault -- Skipping"
        puts ex.inspect     
        info[:error] = ex.inspect
    end
 end
