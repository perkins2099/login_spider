require "spidr"

urls = []
FLAG = ['login','signin']
SKIP_HOST = ['facebook.com','www.facebook.com','twitter.com','www.twitter.com']

# Spidr.start_at('https://app.infoactive.co/login') do |spider|
#   spider.every_html_page do |page|
# 	title =  page.title || ""
# 	puts "[-] #{page.url}".ljust(50) + title
# 	url = page.url.to_s.downcase
	
# 	FLAG.each do |f|
# 		if url.include?(f)
# 		  	urls << url
# 		   	puts "VICTORY"
# 		end
# 	end
#   end

#   puts urls
# end
#http://betali.st/
Spidr.start_at('https://app.infoactive.co/login') do |spider|
  spider.every_url do |url|
	spider.skip_link! if SKIP_HOST.include? (url.host)
	
	puts url.to_s	
	FLAG.each do |f|
		if url.to_s.downcase.include?(f)
		  	urls << url
		   	puts "VICTORY"
		end
	end
  end

  puts urls
end